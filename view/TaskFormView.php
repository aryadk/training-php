
<?php
$detail = @$detail[0];
$id = @$detail['id'];
$url = 'http://localhost/training-php/routes/task/index.php';
$url .= "?act=" . ($id ? "update" : "save");
$csrf_token = $_COOKIE['csrf_token'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo $url?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="csrf_token" value="<?php echo $csrf_token?>" />
        <input type="hidden" name="id" value="<?php echo @$detail['id']?>" />
        <div>
            <label>Nama</label><br>
            <input name="nama" value="<?php echo @$detail['nama']?>" />
        </div>
        <br>
        <div>
            <label>Deskripsi</label><br>
            <input name="deskripsi" value="<?php echo @$detail['deskripsi']?>" />
        </div>
        <br>
        <div>
            <label>Status</label><br>
            <select name="status" value="<?php echo @$detail['status']?>">
                <option value="inprogress">Inprogress </option>
                <option value="done"> Done</option>
            </select>
        </div>
        <br>
        <div>
            <label>Attachment</label><br>
            <input name="attachment" type="file"/>
        </div>
        <br>
        <input type="submit"/>
    </form>

</body>
</html>