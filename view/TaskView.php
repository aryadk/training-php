
<?php
if(isset($detail)) var_dump($detail);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        td,th {
            border: 1px solid #aaa;
            padding: 10px;
        }
    </style>
</head>
<body>
<a href="/training-php/routes/task/index.php?act=detail">Add</a> 
<a href="/training-php/routes/auth/index.php?act=logout">Logout</a> 
    <table>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Status</th>
            <th>Attachment</th>
            <th>Aksi</th>
        </tr>
        <?php foreach($data as $row) : ?>
        <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['nama']; ?></td>
            <td><?php echo $row['deskripsi']; ?></td>
            <td><?php echo $row['status']; ?></td>
            <td><?php echo $row['attachment']; ?></td>
            <td>
                <a href="/training-php/routes/task/index.php?act=detail&id=<?php echo $row['id']; ?>">Edit</a> 
            | <a href="/training-php/routes/task/index.php?act=delete&id=<?php echo $row['id']; ?>">Delete</a></td>
        </tr>
        <?php endforeach;?>

    </table>

</body>
</html>