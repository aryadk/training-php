<?php
require '../../index.php';

$controller = new TrainingPHP\Controller\AuthController();

if(!isset($_GET['act'])){
    echo $controller->index();
    die;
}
switch ($_GET['act']) {
    case 'login':
        echo $controller->login();
        break;

    case 'logout':
        echo $controller->logout();
        break;
    default :
        echo $controller->index();
        break;
}

