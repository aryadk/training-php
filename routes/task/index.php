<?php
require '../../index.php';

$controller = new TrainingPHP\Controller\TaskController();
if(isset($_GET['act'])){
    switch ($_GET['act']) {
        case 'detail':
            echo $controller->detail();
            break;
        case 'save':
            echo $controller->save();
            break;
        case 'update':
            echo $controller->update();
            break;
        case 'delete':
            echo $controller->delete();
            break;
    }
}else{
    echo $controller->index();
}

