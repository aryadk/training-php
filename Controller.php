<?php

namespace TrainingPHP;

class Controller
{
    public $request_headers;
    public function getResponse($code = 200, $info = null, $data = null, $json = true): string | array
    {
        if(!$json){
            return [
                'code' => $code,
                'info' => $info,
                'data' => $data,
            ];
        }
        return json_encode([
            'code' => $code,
            'info' => $info,
            'data' => $data,
        ]);
    }

    public function getRequestBody(): array
    {
        $data = $_POST;
        foreach ($data as $key => $value) {
            $data[$key] = $this->test_input($value);
        }

        return $data;
    }

    public function getRequestParameter(): array
    {
        $data = $_GET;
        foreach ($data as $key => $value) {
            $data[$key] = $this->test_input($value);
        }

        return $data;
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function setHeader()
    {
        if (!function_exists('getallheaders')) {
            foreach ($_SERVER as $name => $value) {
                /* RFC2616 (HTTP/1.1) defines header fields as case-insensitive entities. */
                if (strtolower(substr($name, 0, 5)) == 'http_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
            $this->request_headers = $headers;
        } else {
            $this->request_headers = getallheaders();
        }
    }

    public function fileValidation($target_file)
    {
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "pdf" && $imageFileType != "word" && $imageFileType != "xls"

        ) {
            return $this->getResponse(400, 'Ekstensi file tidak diizinkan');
        }
        return $this->getResponse();
    }

    public function validateAuth()
    {
        if (!(isset($_COOKIE['token']) && isset($_COOKIE['email']))) {
            http_response_code(403);
            die('Forbidden');
        }
        $parameter = [
            'token' => $_COOKIE['token'],
            'email' => $_COOKIE['email']
        ];
        $user = new \TrainingPHP\Model\User();
        return $user->validateAuth($parameter);
    }

    public function validateCsrf(){
        $parameter = $this->getRequestBody();
        if(!(isset($parameter['csrf_token']) && isset($_COOKIE['token']))){
            http_response_code(403);
            die('Forbidden');
        }

        if($parameter['csrf_token'] != isset($_COOKIE['token'])){
            http_response_code(403);
            die('Forbidden');
        }
    }
}
