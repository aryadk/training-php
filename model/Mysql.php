<?php
namespace TrainingPHP\Model;

use Throwable;

class Mysql implements Mysql_I{
    public $connect;
    function __construct(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "task_management";
        $port = "3306";
        try{
            $this->connect = new \mysqli($servername, $username, $password, $database, $port);
            if ($this->connect->connect_error) {
                die("Connection failed: " . $this->connect->connect_error);
            }
        }catch(Throwable $error){
            die($error->getMessage());
        }
    }

    public function select($sql) : array{
        
        $rows = [];
        $result = $this->connect->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        }
        return $rows;
    }

    public function execute($sql) : bool{
        return $this->connect->query($sql);
    }

    public function validateParameter($param): array{
        $parameter = [];
        foreach($param as $key => $item){
            $parameter[$key] = $this->connect->real_escape_string($item);
        }

        return $parameter;
    }
}
