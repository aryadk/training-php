<?php
namespace TrainingPHP\Model;

interface Task_I {

    public function load($param) : array;

    public function save($param) : bool;

    public function update($param) : bool;

    public function delete($param) : bool;
}
