<?php
namespace TrainingPHP\Model;

interface Mysql_I {

    public function select($sql) : array;

    public function execute($sql) : bool;

    public function validateParameter($param) : array;
}
