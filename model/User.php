<?php

namespace TrainingPHP\Model;

class User
{

    public function load($param = []): array
    {
        $db = new Mysql();
        $param = $db->validateParameter($param);
        $query = "SELECT * FROM user";
        if (isset($param['email']) && isset($param['password'])) {
            $email = $param['email'];
            $password = md5($param['password']);
            $query = "SELECT * FROM user WHERE email = '$email' AND password = '$password'";
        }
        return $db->select($query);
    }

    public function validateAuth($param = []): bool
    {
        $db = new Mysql();
        $param = $db->validateParameter($param);
        $query = "SELECT * FROM user";
        if (isset($param['email']) && isset($param['token'])) {
            $email = $param['email'];
            $token = $param['token'];
            $query = "SELECT * FROM user WHERE email = '$email' AND token = '$token'";
        }
        $result = $db->select($query); 
        return isset($result[0]['email']);
    }

    public function authenticate($param = []): bool | string
    {
        $login = $this->load($param);

        if ($login) {
            // $httpAuth = $this->httpAuth([$login[0]['email'] => $login[0]['password']]);
            // if(!$httpAuth['status']){
            //     return false;
            // }
            $login[0]['token'] = $this->generate_token($login[0]['email']);
            if ($this->update($login[0])) {
                $cookie_name = "token";
                $cookie_value = $login[0]['token'];
                setcookie('csrf_token', md5(uniqid(mt_rand(), true)), time() + (86400 * 30), "/");
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
                setcookie('email', $login[0]['email'], time() + (86400 * 30), "/");
                return $login[0]['token'];
            };
        }
        return false;
    }

    public function httpAuth($users){
        $realm = 'Restricted area';

        //user => password
        $users = array('admin' => 'mypass', 'guest' => 'guest');


        if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Digest realm="'.$realm.
                '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

            die();
        }


        // analyze the PHP_AUTH_DIGEST variable
        if (!($data = $this->http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
            !isset($users[$data['username']]))
            return [
                'status' => false,
                'message' => 'Wrong Credentials!'
            ];


        // generate the valid response
        $A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
        $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

        if ($data['response'] != $valid_response)
            return [
                'status' => false,
                'message' => 'Wrong Credentials!'
            ];

        // ok, valid username & password
        return [
            'status' => true,
            'message' => 'ok'
        ];


        
    }

    private function http_digest_parse($txt)
    {
        // protect against missing data
        $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
        $data = array();
        $keys = implode('|', array_keys($needed_parts));

        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        }

        return $needed_parts ? false : $data;
    }

    public function update($param): bool
    {
        $db = new Mysql();
        $param = $db->validateParameter($param);
        $query = sprintf("UPDATE user SET `email` = '%s', `password` ='%s', `token`='%s' WHERE id = %f ", $param['email'], $param['password'], $param['token'], $param['id']);
        
        return $db->execute($query);
    }

    public function generate_token($email): string
    {
        return md5($email . date('ymdhis'));
    }
}
