<?php
namespace TrainingPHP\Model;

class Task implements Task_I{

    public function load($param = []) : array{
        $db = new Mysql();
        $query = "SELECT * FROM task";
        if (isset($param['id'])) {
            $id = $param['id'];
            $query = "SELECT * FROM task WHERE id = $id";
        }
        return $db->select($query);
    }

    public function save($param) : bool{
        $db = new Mysql();
        $param = $db->validateParameter($param);
        $query = sprintf("INSERT INTO task (nama, deskripsi, status, attachment) VALUES ('%s','%s','%s','%s')",$param['nama'],$param['deskripsi'],$param['status'],$param['attachment']);
        return $db->execute($query);
    }

    public function update($param) : bool{
        $db = new Mysql();
        $param = $db->validateParameter($param);
        $query = sprintf("UPDATE task SET nama = '%s', deskripsi='%s', attachment='%s', status='%s' WHERE id = %f ",$param['nama'],$param['deskripsi'],$param['attachment'],$param['status'],$param['id']);
        return $db->execute($query);
    }

    public function delete($id) : bool{
        $db = new Mysql();
        $id = $db->connect->real_escape_string($id);
        $query = "DELETE FROM task WHERE id = $id";
        return $db->execute($query);
    }
}
