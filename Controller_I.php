<?php
namespace TrainingPHP;

interface Controller_I{
    public function getResponse($code,$info,$data) : string;

    public function getRequestBody() : array;

    public function getRequestParameter(): array;
}