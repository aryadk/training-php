<?php

namespace TrainingPHP\Controller;

class AuthController extends \TrainingPHP\Controller implements AuthController_I
{

    public function __construct()
    {
        $this->setHeader();
    }

    public function index(){
        // $token = $_SESSION["token"];
        // die($token);
        $url = "http://localhost/training-php/routes/auth/index.php?act=login";
        require_once(dirname(__DIR__, 1) . '/view/LoginView.php');
    }

    public function login(): string
    {
        $parameter = $this->getRequestBody();

        $user = new \TrainingPHP\Model\User();
        $login = $user->authenticate($parameter);

        if (isset($this->request_headers['Accept']) && $this->request_headers['Accept'] == 'application/json') {
            if($login){
                return $this->getResponse(200,'sukses',['token'=>$login]);
            }else{
                return $this->getResponse(400,'email / password salah');
            }
        } else {
            if($login){
                header('Location: ' . '/training-php/routes/task/index.php');
            }else{
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }


    }

    public function logout()
    {
        unset($_COOKIE['token']);
        setcookie('token', '', time() - 3600, '/');
        unset($_COOKIE['csrf_token']);
        setcookie('csrf_token', '', time() - 3600, '/');
        unset($_COOKIE['email']);
        setcookie('email', '', time() - 3600, '/');
        if (isset($this->request_headers['Accept']) && $this->request_headers['Accept'] == 'application/json') {
            return $this->getResponse();
        }
        header('Location: ' . '/training-php/routes/auth/index.php');
    }
}
