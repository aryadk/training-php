<?php
namespace TrainingPHP\Controller;

interface TaskController_I{
    public function index() ;
    public function save() ;
    public function update();
    public function delete() : void;
}