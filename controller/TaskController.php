<?php

namespace TrainingPHP\Controller;

class TaskController extends \TrainingPHP\Controller implements TaskController_I
{
    public function __construct()
    {
        $this->setHeader();
    }

    public function index()
    {
        $this->validateAuth();
        $task = new \TrainingPHP\Model\Task();
        $data = $task->load();
        if (isset($this->request_headers['Accept']) && $this->request_headers['Accept'] == 'application/json') {
            return $this->getResponse(200, 'sukses', $data);
        } else {
            require_once(dirname(__DIR__, 1) . '/view/TaskView.php');
        }
    }

    public function detail(): void
    {
        $this->validateAuth();
        $task = new \TrainingPHP\Model\Task();
        $parameter = $this->getRequestParameter();
        $detail = isset($parameter['id']) ? $task->load($parameter) : [];
        require_once(dirname(__DIR__, 1) . '/view/TaskFormView.php');

        // return $this->getResponse(200,'sukses',$data);
    }


    function save()
    {
        $this->validateAuth();
        $this->validateCsrf();
        $parameter = $this->getRequestBody();
        

        $upload = $this->uploadFile();
        $task = new \TrainingPHP\Model\Task();
        
        if (isset($this->request_headers['Accept']) && $this->request_headers['Accept'] == 'application/json') {
            if($upload['code'] !== 200){
                return json_encode($upload);
            }
            $parameter['attachment'] = $upload['data']['path'];
            if ($task->save($parameter)) {
                return $this->getResponse(200, 'sukses');
            } else {
                return $this->getResponse(400, 'gagal');
            }
        } else {
            if($upload['code'] !== 200){
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
            $parameter['attachment'] = $upload['data']['path'];
            if ($task->save($parameter)) {
                header('Location: ' . '/training-php/routes/task/index.php');
            } else {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function update()
    {
        $this->validateAuth();
        $this->validateCsrf();
        $parameter = $this->getRequestBody();
        $task = new \TrainingPHP\Model\Task();
        $upload = $this->uploadFile();
        if (isset($this->request_headers['Accept']) && $this->request_headers['Accept'] == 'application/json') {
            if($upload['code'] !== 200){
                return json_encode($upload); 
            }
            $parameter['attachment'] = $upload['data']['path']; 
            if ($task->update($parameter)) {
                return $this->getResponse(200, 'sukses');
            } else {
                return $this->getResponse(400, 'gagal');
            }
        } else {
            if($upload['code'] !== 200){
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
            $parameter['attachment'] = $upload['data']['path'];
            if ($task->update($parameter)) {
                header('Location: ' . '/training-php/routes/task/index.php');
            } else {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function delete(): void
    {
        $this->validateAuth();
        $parameter = $this->getRequestParameter();
        $task = new \TrainingPHP\Model\Task();
        if ($task->delete($parameter['id'])) {
            header('Location: ' . '/training-php/routes/task/index.php');
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    public function uploadFile(){
        $target_dir = dirname(__DIR__, 1)."/uploads/";
        $target_file = $target_dir . basename($_FILES["attachment"]["name"]);

        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if ($_FILES["attachment"]["size"] > 500000) {
            return $this->getResponse(401, 'File terlalu besar',[],false);
        }

        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "pdf" && $imageFileType != "docs" && $imageFileType != "xls"
        ) {
            
            return $this->getResponse(402, 'Ekstensi file tidak diizinkan',[],false);
        }

        if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $target_file)) {
            return $this->getResponse(200, 'ok', ['path' => $target_file],false);
        } else {
            return $this->getResponse(500, 'Failed',[],false);
        }
    }
}
