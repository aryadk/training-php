<?php
namespace TrainingPHP\Controller;

interface AuthController_I{
    public function login() : string;
    public function logout();
}